# U05 Teamprojekt 2 DevOps - API, CI/CD

This project is an e-commerce API for pets. Focus is to create a backend in the form of an API for school purposes. Build an MVP (Minimum Viable Product) by DevOps environment containing at least two CI/CD pipelines(testing and production),and working according to Agile Scrum. We use a database schema and seed data have received from the customer and builds an API in Python.   

# Project Focus
- Testing for API.
- At least 2 pipelines for repo, testing and production in GitLab CI/CD.
- Documentation.
- API and Python.
- Work on projects based on Scrum.
- Trello, which is kept up to date.
- Use Git for version control in a team.

# Project team members
As we have mentioned this project is a group work. Those who were participants in this project are:
- Hassan Jawdat
- Nicklas Thor
- Bernard Bruna
- Jens Bejerskog
- Johnny Agosile
- Ninos Zaiya

# Sprint Plan in Trello
The project consists of 3 sprints of 2 weeks each, plus an initial one sprint zero. In order for this project to be able to have overall and control over it and work agile scrum so what was needed is a sprint plan for each week.This we have done in **Trello** here we attach a link to our board **[Our sprint in Trello](https://trello.com/b/NlCNtqtE/piedpiper-api-doe21?target=_blank)** 

# Technologies Used
Tools we have used in this project
- Fast API.
- Python.
- Docker
- GitLap Pipelines CI/CD.
- Trello (agile scrum).
- Psycopg(Database).



# Testning
 Has made Testing by using **Pytest** framework and Coverage.py which is a tool for measuring code coverage to ensure our API is working as expected. **Pytest** framework makes it easy to write small, readable tests, and can scale to support complex functional testing for applications and libraries.

### *here is our result when we ran pytest with coverage tools locally*.

```
Name                                                                       Stmts   Miss  Cover   Missing
--------------------------------------------------------------------------------------------------------
/home/ninos/Desktop/DevOps_2021/u05-teamprojekt-2-devops/src/__init__.py       0      0   100%
/home/ninos/Desktop/DevOps_2021/u05-teamprojekt-2-devops/src/shop_api.py     183      2    99%   19, 27
__init__.py                                                                    0      0   100%
api_test_mock.py                                                             232      0   100%
--------------------------------------------------------------------------------------------------------
TOTAL                                                                        415      2    99%

```

# Documentation
We have documented our work and all sprinters and put it in  a document you can check it out in here **[The document](https://gitlab.com/Geard8/u05-teamprojekt-2-devops/-/blob/main/_u05%20Teamprojekt%202%20DevOps%20Grupp%20PiedPiper.pdf)** .

# Saker som behöves lägga till i denna readme
- Instruktion om allt som behöves göras för att köra projetet när man clonar ner det nytt ifrån repot. Detta är saker som pip att installer requirements.txt, docker-compuse up -d, fylla i databasen med scheam och seed-data, hur man göra ett api anropp för att testa att det fungerar. Tänk på vad allt som behövs göra om en främling som en arbets givare vill testa projektet och se att det fungerar och vad man kan göra med.
- Kort info om allt viktigt som är gjort som endpoit, tester, pipeline, pylint/PEP8, docker och eventult mer som är värt att nämna.
- Slut summering där vi skriver ihop en text om hur det varit att jobba med projektet, som kan innehålla saker som vad vi lärt oss, vad vi haft svårtigheter med. Vad kanske vi skulle gjort annorlunda nu med våran nya kunskapa om vi skulle göra om något liknade och annat vi känner för att med.