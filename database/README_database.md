Klad tills vidare, behöves fixa/snyggas till.

Bra saker att komma ihåg för database biten.

- För att start docker för database och pgadmin och fastapi. Behöves göras där docker-compose.yml för databasen.
```
docker-compose up -d
```

- För att köra psql localt mot databasen på docker.
```
psql -h localhost -U postgres
```

- För att lägga till schema och seed data. 
- Behöver vara i database mappen där vi har filern för att fungera.
```
cat u05_schema.sql | psql -h localhost -U postgres
cat u05_seed_stores.sql | psql -h localhost -U postgres
```

- Besök denna länk för att komma in på pgadmin för databasen i docker.

http://localhost:8080/browser/
