""" Docstring """
# pylint: disable=C0301, W0105, R0903, C0116 # Line too long, pointless-string-statement, too-few-public-methods
from unittest.mock import MagicMock, Mock
from types import SimpleNamespace
from fastapi.testclient import TestClient
import psycopg

from src.shop_api import app

def build_test_client(db_data):
    """
    Building mock-data

    """
    # Exepel data: db_data = [["store name", "adress 55", "12345", "city name"]]

    class CursorMock(MagicMock):
        """
        Class to fetch all mocking
        """
        fetchall = Mock(return_value=db_data)
        fetchone = Mock(return_value=db_data)

    class DBMock:
        """
        Docstring
        """
        cursor = CursorMock

    app.db = DBMock()
    return TestClient(app)


def test_get_stores():
    """This tests the response status code and details for a correct output to the
    /stores endpoint.
    """

    client = build_test_client([["store name", "adress 55", "12345", "city name"], ["store name2", "adress 55", "12345", "city name"]])

    response = client.get("/stores")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
            "name": "store name",
            "full_adress": "adress 55 city name 12345"
            },
            {
            "name": "store name2",
            "full_adress": "adress 55 city name 12345"
            }
        ]
}

def test_get_store_by_name_ok():
    client = build_test_client(["store_name", "adress 55", "12345", "city name"])

    response = client.get("/stores/store_name")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
            "name": "store_name",
            "full_adress": "adress 55 city name 12345"
            }
        ]
    }

def test_get_store_not_found():
    client = build_test_client([])

    response = client.get("/stores/not_found")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}

def test_get_cities():
    client = build_test_client([["Cityone"], ["Citytwo"]])

    response = client.get("/cities/")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Cityone",
            "Citytwo",
        ]
    }


def test_get_cities_by_zip_city():
    client = build_test_client([["Cityone"]])

    response = client.get("/cities?zip_city=55555")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Cityone"
        ]
    }

def test_get_cities_wrong_zip_city():
    client = build_test_client([])

    response = client.get("/cities?zip_city=02020")
    assert response.status_code == 200
    assert response.json() == {
  "data": []
}


# #================FRIVILLIGT============================
def test_get_cities_list_by_zip_city():
    client = build_test_client([["Cityone"], ["Citytwo"]])

    response = client.get("/cities/?zip_city=54321&zip_city=54545")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Cityone",
            "Citytwo",
        ]
    }


def test_get_store_by_full():
    client = build_test_client([["store name", "adress 55", "12345", "city name"], ["store name2", "adress 55", "12345", "city name"]])


    response = client.get("/stores/?use_format=full")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
            "name": "store name",
            "full_adress": "adress 55 city name 12345"
            },
            {
            "name": "store name2",
            "full_adress": "adress 55 city name 12345"
            }
        ]
    }

def test_get_store_by_name():
    client = build_test_client([["store name"], ["store name2"]])

    response = client.get("/stores/?use_format=name")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"name": "store name"},
            {"name": "store name2"}
        ]
    }


def test_get_store_by_compact():
    client = build_test_client([["store name", "city name"], ["store name2", "city name"]])

    response = client.get("/stores/?use_format=compact")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "store name",
                "city": "city name"
            },
            {
                "name": "store name2",
                "city": "city name"
            }
        ]
    }

def test_get_store_by_full_with_path_name():
    client = build_test_client(["store_name", "adress 55", "12345", "city name"])

    response = client.get("/stores/store_name/?use_format=full")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
            "name": "store_name",
            "full_adress": "adress 55 city name 12345"
            }
        ]
    }

def test_get_store_by_name_with_path_name():
    client = build_test_client(["store_name"])

    response = client.get("/stores/store_name/?use_format=name")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"name": "store_name"}
        ]
    }


def test_get_store_by_wrong_name():
    client = build_test_client([])

    response = client.get("/stores/Djurjourens/?use_format=name")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}


def test_get_store_by_wrong_compact():
    client = build_test_client([])

    response = client.get("/stores/Djurjourens/?use_format=compact")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}


def test_get_store_by_compact_with_path_name():
    client = build_test_client(["store_name", "city name"])

    response = client.get("/stores/store_name/?use_format=compact")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "store_name",
                "city": "city name"
            }
        ]
    }

def test_get_store_with_wrong_format():
    client = build_test_client([])

    response = client.get("/stores/?use_format=wrong")
    assert response.status_code == 422
    assert response.json() == {"detail": "Wrong format request"}

def test_get_store_with_wrong_format_with_path_name():
    client = build_test_client([])

    response = client.get("/stores/Djurjouren/?use_format=wrong")
    assert response.status_code == 422
    assert response.json() == {"detail": "Wrong format request"}

def test_get_sales():
    client = build_test_client([["store_name", "2022-01-25 13:52:34", "0188146f-5360-408b-a7c5-3414077ceb59"], ["store_name2", "2022-01-26 15:24:45", "726ac398-209d-49df-ab6a-682b7af8abfb"]])


    response = client.get("/sales/")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"store":"store_name","timestamp":"20220125T13:52:34","sale_id":"0188146f-5360-408b-a7c5-3414077ceb59"},
            {"store":"store_name2","timestamp":"20220126T15:24:45","sale_id":"726ac398-209d-49df-ab6a-682b7af8abfb"}
        ]
    }

def test_get_sales_by_id():
    client = build_test_client([["store_name", "2022-01-25 13:52:34", "0188146f-5360-408b-a7c5-3414077ceb59", "pet toy 1", 1], ["store_name", "2022-01-25 13:52:34", "0188146f-5360-408b-a7c5-3414077ceb59", "pet toy 2", 2]])


    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb59")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "store":"store_name",
                "timestamp":"20220125T13:52:34",
                "sale_id":"0188146f-5360-408b-a7c5-3414077ceb59",
                "products":[
                    {"name":"pet toy 1","qty": 1},
                    {"name":"pet toy 2","qty": 2}
                ]
            }
        ]
    }

def test_get_sales_by_id_with_wrong_id():
    client = build_test_client([])

    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb59")
    assert response.status_code == 404
    assert response.json() == {"detail": "Sale not found"}

def test_get_sales_by_id_with_wrong_format():
    client = build_test_client([])
    response = client.get("/sales/wrong-id-format")
    assert response.status_code == 422
    assert response.json() == {"detail": "Wrong format on sales id, it need to be uuid valid"}


def test_get_income():
    client = build_test_client([["store name", "cat toy", 99, 123, "2022-01-25T13:52:34", 50], ["store name2", "cat toy2", 99, 123, "2022-01-25T13:52:34", 50]])

    response = client.get("/income")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"store name","product_name":"cat toy","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
            {"store_name":"store name2","product_name":"cat toy2","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
        ]
   }

def test_get_income_by_from():
    client = build_test_client([["store name", "cat toy", 99, 123, "2022-01-25T13:52:34", 50], ["store name2", "cat toy2", 99, 123, "2022-01-25T13:52:34", 50]])

    response = client.get("/income?from=2022-01-25T13:52:33")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"store name","product_name":"cat toy","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
            {"store_name":"store name2","product_name":"cat toy2","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
        ]
    }

def test_get_income_by_to():
    client = build_test_client([["store name", "cat toy", 99, 123, "2022-01-25T13:52:34", 50], ["store name2", "cat toy2", 99, 123, "2022-01-25T13:52:34", 50]])

    response = client.get("/income?to=2022-01-25T13:52:35")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"store name","product_name":"cat toy","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
            {"store_name":"store name2","product_name":"cat toy2","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
        ]
    }

def test_get_income_by_from_wrong_format():
    # Need for specil case where fetchall need to raise a specific error
    def build_test_client(db_data):
        # Exepel data: db_data = [["store name", "adress 55", "12345", "city name"]]
        

        class CursorMock(MagicMock):
            def fetchall(*args):
                raise psycopg.errors.Error

        class DBMock:
            cursor = CursorMock

            def rollback(*args):
                "Exist for code runs it but need not do anything for this test"
                pass

        app.db = DBMock()
        return TestClient(app)

    client = build_test_client([])

    response = client.get("/income?from=2022")
    assert response.status_code == 422
    assert response.json() == {"detail":"Invalid datetime format!"}


def test_get_income_by_stores():
    client = build_test_client([["store name", "cat toy", 99, 123, "2022-01-25T13:52:34", 50], ["store name2", "cat toy2", 99, 123, "2022-01-25T13:52:34", 50]])

    response = client.get("/income?store=ff53d831-c2fe-4fe8-9f67-5d69118670f2&store=676df1a1-f1d1-4ac5-9ee3-c58dfe820927")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"store name","product_name":"cat toy","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
            {"store_name":"store name2","product_name":"cat toy2","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
        ]
    }

def test_get_income_by_store_wrong_format():
    client = build_test_client([])

    response = client.get("/income?store=ff53d831-")
    assert response.status_code == 422
    assert response.json() == {"detail":"Invalid UUID given for store!"}

def test_get_income_by_products():
    client = build_test_client([["store name", "cat toy", 99, 123, "2022-01-25T13:52:34", 50], ["store name2", "cat toy2", 99, 123, "2022-01-25T13:52:34", 50]])

    response = client.get("/income?product=a37c34ae-0895-484a-8b2a-355aea3b6c44&product=02d9562f-05e8-49b2-8ace-6e166c440060")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"store name","product_name":"cat toy","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
            {"store_name":"store name2","product_name":"cat toy2","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
        ]
    }

def test_get_income_by_product_wrong_format():
    client = build_test_client([])

    response = client.get("/income?product=a37c34ae-")
    assert response.status_code == 422
    assert response.json() == {"detail":"Invalid UUID given for product!"}

def test_get_income_by_all_parameters():
    client = build_test_client([["store name", "cat toy", 99, 123, "2022-01-25T13:52:34", 50], ["store name2", "cat toy2", 99, 123, "2022-01-25T13:52:34", 50]])

    response = client.get("/income?product=02d9562f-05e8-49b2-8ace-6e166c440060&store=676df1a1-f1d1-4ac5-9ee3-c58dfe820927&from=2022-02-27T12:32:45&to=2022-02-27T12:32:47  ")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"store name","product_name":"cat toy","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
            {"store_name":"store name2","product_name":"cat toy2","price":99,"quantity":123,"sale_time":"2022-01-25T13:52:34","discount":50},
        ]
    }


"""Here are unit tests for the GET /inventory endpoint. You should be able to
run the tests as they are, or with minimal edits once the endpoint is
integrated into your codebase.

The logic in the tests is correct. If a test fails, there is a bug in the
endpoint. So ***you should not*** change the logic in the test, but in the
endpoint to make the tests pass.
"""

all_inventories = [
    [
        "Hundmat",
        27,
        "Den Lilla Djurbutiken"
    ],
    [
        "Kattmat",
        387,
        "Den Lilla Djurbutiken"
    ],
    [
        "Hundmat",
        140,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattklonare",
        68,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattmat",
        643,
        "Den Stora Djurbutiken"
    ],
    [
        "Sömnpiller och energidryck för djur",
        61,
        "Den Stora Djurbutiken"
    ],
    [
        "Elefantkoppel",
        27,
        "Djuristen"
    ],
]

return_data = [
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 27,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 387,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 140,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattklonare",
    "adjusted_quantity": 68,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 643,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Sömnpiller och energidryck för djur",
    "adjusted_quantity": 61,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Elefantkoppel",
    "adjusted_quantity": 27,
    "store_name": "Djuristen"
  }
]

def db_mock(data):
    """This function returns a database mocking object, that will be used
    instead of the actual db connection.
    """
    database = SimpleNamespace()
    database.cursor = CursorMocking(data)
    return database

class CursorMocking:
    """This class mocks a db cursor. It does not build upon unittest.mock but
    it is instead built from an empty class, patching manually all needed
    methods.
    """
    def __init__(self, data):
        self.data = data

    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def __call__(self):
        return self

    @staticmethod
    def execute(*args):
        """This mocks cursor.execute. It returns args even though the return
        value of cursor.execute() is never used. This is to avoid the
        following linting error:

        W0613: Unused argument 'args' (unused-argument)
        """
        return args

    def fetchall(self):
        """This mocks cursor.fetchall.
        """
        return self.data


def test_get_inventory():
    """This unit test checks a call to GET /inventory without any query
    parameters.
    """
    app.db = db_mock(all_inventories)
    client = TestClient(app)
    response = client.get("/inventory")
    assert response.status_code == 200
    assert response.json() == return_data


def test_get_inventory_store():
    """This unit test checks a call to GET /inventory?store=UUID
    """
    data = list(filter(lambda x: x[-1] == "Den Stora Djurbutiken",
                       all_inventories))
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={
                              "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927"})
    assert response.status_code == 200
    assert response.json() == list(filter(
        lambda x: x["store_name"] == "Den Stora Djurbutiken", return_data))

def test_get_inventory_product():
    """This unit test checks a call to GET /inventory?product=UUID
    """
    data = list(filter(lambda x: x[0] == "Hundmat", all_inventories))
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={
                              "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44"
                          })
    assert response.status_code == 200
    assert response.json() == list(filter(
        lambda x: x["product_name"] == "Hundmat", return_data))


def test_get_inventory_store_and_product():
    """This unit test checks a call to GET /inventory?store=UUID&product=UUID
    """
    data = list(filter(
        lambda x: x[0] == "Hundmat" and x[-1] == "Den Stora Djurbutiken",
        all_inventories))
    app.db = db_mock(data)
    client = TestClient(app)
    response = client.get("/inventory", params={
        "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44",
        "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927"
    })
    assert response.status_code == 200
    assert response.json() == list(
        filter(
            lambda x: x["store_name"] == "Den Stora Djurbutiken" and
                      x["product_name"] == "Hundmat", return_data))


def test_get_inventory_erroneous_store():
    """This unit test checks for a call to GET /inventory?store=Erroneous-UUID
    """
    app.db = db_mock(None)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={"store": "this is not a valid UUID!"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID for store!"}


def test_get_inventory_erroneous_product():
    """This unit test checks for a call to GET /inventory?product=Erroneous-UUID
    """
    app.db = db_mock(None)
    client = TestClient(app)
    response = client.get("/inventory",
                          params={"product": "this is not a valid UUID!"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID for product!"}
