# pylint: disable=C0301, W0105 # Line too long, pointless-string-statement
"""tests for shop_api.py
"""
from types import SimpleNamespace
from fastapi.testclient import TestClient
#import psycopg


#import the dbpass.py file with database connection
from src.shop_api import app
from .dbpass import database_credentials


app.db = database_credentials()

client = TestClient(app)


def test_get_stores():
    "test stores to get all stores"
    response = client.get("/stores")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
            "name": "Djurjouren",
            "full_adress": "Upplandsgatan 99 Stockholm 12345"
            },
            {
            "name": "Djuristen",
            "full_adress": "Skånegatan 420 Falun 54321"
            },
            {
            "name": "Den Lilla Djurbutiken",
            "full_adress": "Nätverksgatan 22 Hudiksvall 55555"
            },
            {
            "name": "Den Stora Djurbutiken",
            "full_adress": "Routergatan 443 Hudiksvall 54545"
            },
            {
            "name": "Noahs Djur & Båtaffär",
            "full_adress": "Stallmansgatan 666 Gävle 96427"
            }
        ]
}


def test_get_store_by_name_ok():
    "test stores with store name in path"
    response = client.get("/stores/Djuristen")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
            "name": "Djuristen",
            "full_adress": "Skånegatan 420 Falun 54321"
            }
        ]
    }


def test_get_store_not_found():
    "test stores with store name in path that do not exist"
    response = client.get("/stores/zoo")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}


def test_get_cities():
    "test cities to get all cities"
    response = client.get("/cities/")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Gävle",
            "Falun",
            "Stockholm",
            "Hudiksvall"
        ]
    }


def test_get_cities_by_zip_city():
    "test cities with one zip_city"
    response = client.get("/cities?zip_city=55555")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Hudiksvall"
        ]
    }


def test_get_cities_wrong_zip_city():
    "test cities with one zip_city what has no city"
    response = client.get("/cities?zip_city=02020")
    assert response.status_code == 200
    assert response.json() == {
  "data": []
}


def test_get_cities_list_by_zip_city():
    "test cities with many zip_city"
    response = client.get("/cities/?zip_city=54321&zip_city=54545&zip_city=96427")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Hudiksvall",
            "Gävle",
            "Falun"
        ]
    }


def test_get_store_by_full():
    "test stores with use_format is full"
    response = client.get("/stores/?use_format=full")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "full_adress": "Upplandsgatan 99 Stockholm 12345"
            },
            {
                "name": "Djuristen",
                "full_adress": "Skånegatan 420 Falun 54321"
            },
            {
                "name": "Den Lilla Djurbutiken",
                "full_adress": "Nätverksgatan 22 Hudiksvall 55555"
            },
            {
                "name": "Den Stora Djurbutiken",
                "full_adress": "Routergatan 443 Hudiksvall 54545"
            },
            {
                "name": "Noahs Djur & Båtaffär",
                "full_adress": "Stallmansgatan 666 Gävle 96427"
            }
        ]
    }

def test_get_store_by_name():
    "test stores with use_format is name"
    response = client.get("/stores/?use_format=name")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"name": "Djurjouren"},
            {"name": "Djuristen"},
            {"name": "Den Lilla Djurbutiken"},
            {"name": "Den Stora Djurbutiken"},
            {"name": "Noahs Djur & Båtaffär"}
        ]
    }


def test_get_store_by_compact():
    "test stores with use_format is compact"
    response = client.get("/stores/?use_format=compact")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "city": "Stockholm"
            },
            {
                "name": "Djuristen",
                "city": "Falun"
            },
            {
                "name": "Den Lilla Djurbutiken",
                "city": "Hudiksvall"
            },
            {
                "name": "Den Stora Djurbutiken",
                "city": "Hudiksvall"
            },
            {
                "name": "Noahs Djur & Båtaffär",
                "city": "Gävle"
            }
        ]
    }

def test_get_store_by_full_with_path_name():
    "test stores with store name path and use_format is full"
    response = client.get("/stores/Djurjouren/?use_format=full")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "full_adress": "Upplandsgatan 99 Stockholm 12345"
            }
        ]
    }

def test_get_store_by_name_with_path_name():
    "test stores with store name path and use_format is name"
    response = client.get("/stores/Djurjouren/?use_format=name")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"name": "Djurjouren"}
        ]
    }

def test_get_store_by_wrong_name():
    "test stores with store name path and use_format is name where store name dont exist"
    response = client.get("/stores/Djurjourens/?use_format=name")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}


def test_get_store_by_wrong_compact():
    "test stores with store name path and use_format is compact where store name dont exist"
    response = client.get("/stores/Djurjourens/?use_format=compact")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}

def test_get_store_by_compact_with_path_name():
    "test stores with use_format is compact"
    response = client.get("/stores/Djurjouren/?use_format=compact")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "city": "Stockholm"
            }
        ]
    }

def test_get_store_with_wrong_format():
    "test stores with use_format is wrong"
    response = client.get("/stores/?use_format=wrong")
    assert response.status_code == 422
    assert response.json() == {"detail": "Wrong format request"}

def test_get_store_with_wrong_format_with_path_name():
    "test stores with store name in path and use_format is wrong"
    response = client.get("/stores/Djurjouren/?use_format=wrong")
    assert response.status_code == 422
    assert response.json() == {"detail": "Wrong format request"}

def test_get_sales():
    "test sales"
    response = client.get("/sales/")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"store":"Den Stora Djurbutiken","timestamp":"20220125T13:52:34", "sale_id":"0188146f-5360-408b-a7c5-3414077ceb59"},

            {"store":"Djuristen","timestamp":"20220126T15:24:45", "sale_id":"726ac398-209d-49df-ab6a-682b7af8abfb"},

            {"store":"Den Lilla Djurbutiken","timestamp":"20220207T09:00:56", "sale_id":"602fbf9d-2b4a-4de2-b108-3be3afa372ae"},

            {"store":"Den Stora Djurbutiken","timestamp":"20220227T12:32:46", "sale_id":"51071ca1-0179-4e67-8258-89e34b205a1e"}
        ]
    }

def test_get_sales_by_id():
    "test sales with sale id"
    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb59")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "store":"Den Stora Djurbutiken",
                "timestamp":"20220125T13:52:34",
                "sale_id":"0188146f-5360-408b-a7c5-3414077ceb59",
                "products":[
                    {"name":"Hundmat","qty":3},
                    {"name":"Sömnpiller och energidryck för djur","qty":12}
                ]
            }
        ]
    }

def test_get_sales_by_id_with_wrong_id():
    "test sales with sale id what do not exist"
    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb50")
    assert response.status_code == 404
    assert response.json() == {"detail": "Sale not found"}

def test_get_sales_by_id_with_wrong_format():
    "test sales with wrong format on sale id"
    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb5")
    assert response.status_code == 422
    assert response.json() == {"detail": "Wrong format on sales id, it need to be uuid valid"}

def test_get_income():
    "test income without any paramenters"
    response = client.get("/income")
    assert response.status_code == 200
    assert response.json() == {
        "data" :[
            {"store_name":"Den Stora Djurbutiken","product_name":"Sömnpiller och energidryck för djur","price":9.95,"quantity":12,"sale_time":"2022-01-25T13:52:34","discount":9},
            {"store_name":"Den Stora Djurbutiken","product_name":"Hundmat","price":109,"quantity":3,"sale_time":"2022-01-25T13:52:34","discount":None},
            {"store_name":"Djuristen","product_name":"Elefantkoppel","price":459,"quantity":1,"sale_time":"2022-01-26T15:24:45","discount":13},
            {"store_name":"Den Lilla Djurbutiken","product_name":"Kattmat","price":109,"quantity":57,"sale_time":"2022-02-07T09:00:56","discount":None},
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattklonare","price":55900,"quantity":1,"sale_time":"2022-02-27T12:32:46","discount":25},
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattmat","price":109,"quantity":10,"sale_time":"2022-02-27T12:32:46","discount":None}
        ]
    }

def test_get_income_by_from():
    "test income with from"
    response = client.get("/income?from=2022-02-27T12:32:45")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattklonare","price":55900,"quantity":1,"sale_time":"2022-02-27T12:32:46","discount":25},
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattmat","price":109,"quantity":10,"sale_time":"2022-02-27T12:32:46","discount":None}
        ]
    }

def test_get_income_by_to():
    "test income with to"
    response = client.get("/income?to=2022-02-27T12:32:45")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"store_name":"Den Stora Djurbutiken","product_name":"Sömnpiller och energidryck för djur","price":9.95,"quantity":12,"sale_time":"2022-01-25T13:52:34","discount":9},
            {"store_name":"Den Stora Djurbutiken","product_name":"Hundmat","price":109,"quantity":3,"sale_time":"2022-01-25T13:52:34","discount":None},
            {"store_name":"Djuristen","product_name":"Elefantkoppel","price":459,"quantity":1,"sale_time":"2022-01-26T15:24:45","discount":13},
            {"store_name":"Den Lilla Djurbutiken","product_name":"Kattmat","price":109,"quantity":57,"sale_time":"2022-02-07T09:00:56","discount":None}
        ]
    }

def test_get_income_by_from_wrong_format():
    "test income with wrong format on from"
    response = client.get("/income?from=2022")
    assert response.status_code == 422
    assert response.json() == {"detail":"Invalid datetime format!"}


def test_get_income_by_stores():
    "test income with many store id"
    response = client.get("/income?store=ff53d831-c2fe-4fe8-9f67-5d69118670f2&store=676df1a1-f1d1-4ac5-9ee3-c58dfe820927")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"store_name":"Den Stora Djurbutiken","product_name":"Hundmat","price":109,"quantity":3,"sale_time":"2022-01-25T13:52:34","discount":None},
            {"store_name":"Den Stora Djurbutiken","product_name":"Sömnpiller och energidryck för djur","price":9.95,"quantity":12,"sale_time":"2022-01-25T13:52:34","discount":9},
            {"store_name":"Den Lilla Djurbutiken","product_name":"Kattmat","price":109,"quantity":57,"sale_time":"2022-02-07T09:00:56","discount":None},
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattmat","price":109,"quantity":10,"sale_time":"2022-02-27T12:32:46","discount":None},
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattklonare","price":55900,"quantity":1,"sale_time":"2022-02-27T12:32:46","discount":25}
        ]
    }

def test_get_income_by_store_wrong_format():
    "test income with wrong format on id on store"
    response = client.get("/income?store=ff53d831-")
    assert response.status_code == 422
    assert response.json() == {"detail":"Invalid UUID given for store!"}

def test_get_income_by_products():
    "test income with many product id"
    response = client.get("/income?product=a37c34ae-0895-484a-8b2a-355aea3b6c44&product=02d9562f-05e8-49b2-8ace-6e166c440060")
    assert response.status_code == 200
    assert response.json() == {
        "data":[{"store_name":"Den Stora Djurbutiken","product_name":"Hundmat","price":109,"quantity":3,"sale_time":"2022-01-25T13:52:34","discount":None},
            {"store_name":"Den Lilla Djurbutiken","product_name":"Kattmat","price":109,"quantity":57,"sale_time":"2022-02-07T09:00:56","discount":None},
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattmat","price":109,"quantity":10,"sale_time":"2022-02-27T12:32:46","discount":None}
        ]
    }

def test_get_income_by_product_wrong_format():
    "test income with wrong format on id on product"
    response = client.get("/income?product=a37c34ae-")
    assert response.status_code == 422
    assert response.json() == {"detail":"Invalid UUID given for product!"}

def test_get_income_by_all_parameters():
    "test income with all parameters"
    response = client.get("/income?product=02d9562f-05e8-49b2-8ace-6e166c440060&store=676df1a1-f1d1-4ac5-9ee3-c58dfe820927&from=2022-02-27T12:32:45&to=2022-02-27T12:32:47  ")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {"store_name":"Den Stora Djurbutiken","product_name":"Kattmat","price":109,"quantity":10,"sale_time":"2022-02-27T12:32:46","discount":None}
        ]
    }


"""
Here are unit tests for the GET /inventory endpoint. You should be able to
run the tests as they are, or with minimal edits once the endpoint is
integrated into your codebase.

The logic in the tests is correct. If a test fails, there is a bug in the
endpoint. So ***you should not*** change the logic in the test, but in the
endpoint to make the tests pass.

"""

all_inventories = [
    [
        "Hundmat",
        27,
        "Den Lilla Djurbutiken"
    ],
    [
        "Kattmat",
        387,
        "Den Lilla Djurbutiken"
    ],
    [
        "Hundmat",
        140,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattklonare",
        68,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattmat",
        643,
        "Den Stora Djurbutiken"
    ],
    [
        "Sömnpiller och energidryck för djur",
        61,
        "Den Stora Djurbutiken"
    ],
    [
        "Elefantkoppel",
        27,
        "Djuristen"
    ],
]

return_data = [
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 27,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 387,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 140,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattklonare",
    "adjusted_quantity": 68,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 643,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Sömnpiller och energidryck för djur",
    "adjusted_quantity": 61,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Elefantkoppel",
    "adjusted_quantity": 27,
    "store_name": "Djuristen"
  }
]

def db_mock(data):
    """This function returns a database mocking object, that will be used
    instead of the actual db connection.
    """
    database = SimpleNamespace()
    database.cursor = CursorMock(data)
    return database

class CursorMock:
    """This class mocks a db cursor. It does not build upon unittest.mock but
    it is instead built from an empty class, patching manually all needed
    methods.
    """
    def __init__(self, data):
        self.data = data

    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def __call__(self):
        return self

    @staticmethod
    def execute(*args):
        """This mocks cursor.execute. It returns args even though the return
        value of cursor.execute() is never used. This is to avoid the
        following linting error:

        W0613: Unused argument 'args' (unused-argument)
        """
        return args

    def fetchall(self):
        """This mocks cursor.fetchall.
        """
        return self.data


def test_get_inventory():
    """This unit test checks a call to GET /inventory without any query
    parameters.
    """
    app.db = db_mock(all_inventories)
    client_ = TestClient(app)
    response = client_.get("/inventory")
    assert response.status_code == 200
    assert response.json() == return_data


def test_get_inventory_store():
    """This unit test checks a call to GET /inventory?store=UUID
    """
    data = list(filter(lambda x: x[-1] == "Den Stora Djurbutiken",
                       all_inventories))
    app.db = db_mock(data)
    client_ = TestClient(app)
    response = client_.get("/inventory",
                          params={
                              "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927"})
    assert response.status_code == 200
    assert response.json() == list(filter(
        lambda x: x["store_name"] == "Den Stora Djurbutiken", return_data))

def test_get_inventory_product():
    """This unit test checks a call to GET /inventory?product=UUID
    """
    data = list(filter(lambda x: x[0] == "Hundmat", all_inventories))
    app.db = db_mock(data)
    client_ = TestClient(app)
    response = client_.get("/inventory",
                          params={
                              "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44"
                          })
    assert response.status_code == 200
    assert response.json() == list(filter(
        lambda x: x["product_name"] == "Hundmat", return_data))


def test_get_inventory_store_and_product():
    """This unit test checks a call to GET /inventory?store=UUID&product=UUID
    """
    data = list(filter(
        lambda x: x[0] == "Hundmat" and x[-1] == "Den Stora Djurbutiken",
        all_inventories))
    app.db = db_mock(data)
    client_ = TestClient(app)
    response = client_.get("/inventory", params={
        "product": "a37c34ae-0895-484a-8b2a-355aea3b6c44",
        "store": "676df1a1-f1d1-4ac5-9ee3-c58dfe820927"
    })
    assert response.status_code == 200
    assert response.json() == list(
        filter(
            lambda x: x["store_name"] == "Den Stora Djurbutiken" and
                      x["product_name"] == "Hundmat", return_data))


def test_get_inventory_erroneous_store():
    """This unit test checks for a call to GET /inventory?store=Erroneous-UUID
    """
    app.db = db_mock(None)
    client_ = TestClient(app)
    response = client_.get("/inventory",
                          params={"store": "this is not a valid UUID!"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID for store!"}


def test_get_inventory_erroneous_product():
    """This unit test checks for a call to GET /inventory?product=Erroneous-UUID
    """
    app.db = db_mock(None)
    client_ = TestClient(app)
    response = client_.get("/inventory",
                          params={"product": "this is not a valid UUID!"})
    assert response.status_code == 422
    assert response.json() == {"detail": "Invalid UUID for product!"}
