Klad tills vidare, behöves fixa/snyggas till.

Bra saker att komma ihåg relaterad till pytest.

- För att använd pytest
```
# To run pytest
$ pytest

# Run pytest on a specific file.
python file.py

# Run pytest with the -q/--quiet flag keeps the output brief.
pytest -q file.py
```

- Exepmel of pytest code
```
# content of test_sample.py
def func(x):
    return x + 1

def test_answer():
    assert func(3) == 4
```

- Exepmel of pytest code on fastAPI
```
from fastapi import FastAPI
from fastapi.testclient import TestClient

app = FastAPI()

@app.get("/")
async def read_main():
    return {"msg": "Hello World"}

client = TestClient(app)

def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}
```
