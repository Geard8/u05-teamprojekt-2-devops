Klad tills vidare, behöves fixa/snyggas till.

Bra saker att komma ihåg relaterad till python och det bibliotek som FastAPI och psycopg

- För att använd FastAPI
```
from fastapi import FastAPI basic.
app = FastAPI()

# Basic get endpoint
@app.get("/")
def root():
    return {"message": "Hello World"}
```

- För att använda uvicorn för att ha en liten server för FastAPI. 
```
# DEPRECATED Fungerar inte längre för detta project då det är anpassat för att docker hanter fastapi med uvicorn
# Where file_name is file name like main (do not include .py part)
# Where object_name is object FastAPI i used with from something like "app = FastAPI()" where app here is the object_name.

uvicorn file_name:object_name --reload 
```


- För att använd psycopg basic.
```
import psycopg

# Connect to an existing database
with psycopg.connect("dbname=postgres user=postgres host=localhost password=testpass") as conn:
    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        cur.execute("SELECT * FROM tabel_name")
```

- För att använda FastAPI och psycopg tillsammans.
```
# För att starta database kopplingen när servern för API körs.
@app.on_event("startup")
def startup():
    app.db = psycopg.connect("dbname=postgres user=postgres host=localhost password=testpass")

# För att stänga database kopplingen när servern för API körs.
@app.on_event("shutdown")
def shutdown():
    app.db.close()

# Basic exemel of using database in API.
@app.get("/")
def root():
    with app.db.cursor() as cur:
        cur.execute("SELECT * FROM tabel_name")
        return {"data": cur.fetchall()}

```
