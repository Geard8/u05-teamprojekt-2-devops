"Docstring"
# pylint: disable=C0301, C0103, E1125 # Line too long
# ======================== V 2.0 ===================================================
import datetime
from collections import namedtuple
from typing import List, Optional
import uuid
from fastapi import FastAPI, HTTPException, Query
import psycopg

app = FastAPI()


@app.on_event("startup")
def startup():
    """
    Documentation for this function here
    """
    app.db = psycopg.connect("dbname=postgres user=postgres host=db password=testpass")


@app.on_event("shutdown")
def shutdown():
    """
    Documetation for this function here
    """
    app.db.close()


def is_valid_uuid(value):
    """
    Info here.
    """
    try:
        uuid.UUID(value)

        return True
    except ValueError:
        return False


@app.get("/cities/", tags=['Friviligt'])
def get_cities(zip_city: List[int] = Query(None)):
    """
    GET /cities query parameter zip ska kunna anges fler
    gånger, och då måste alla städer som stämmer överens med en zip vara
    med i resultatet som returneras.
    i.e.: GET /cities?zip=12345&zip=54321
    """
    with app.db.cursor() as cur:
        if zip_city:
            zip_str = [str(x) for x in zip_city]
            cur.execute("SELECT DISTINCT city FROM store_addresses WHERE zip = ANY(%s)", ["{" + ",".join(zip_str) + "}"])
        else:
            cur.execute("SELECT DISTINCT city FROM store_addresses")

        data = {"data": []}
        for row in cur.fetchall():
            data["data"].append(row[0])

        return data


@app.get("/stores/", tags=['Friviligt'])
def get_stores(use_format: Optional[str] = None):
    """
    FRIVILLIGT: ska kunna ta emot en query parameter format som kan enbart vara en av följande värden:
    - full: all info (namn + fullständigt address) returneras
    - name: bara namn på stores returneras
    - compact: bara namn och stad returneras
    Om format anges med något annat värde än de 3 ovan, ett felmeddelande
    ska returneras (status code: 422 Unprocessable Entry). Om format inte anges, anta full.
    """
    with app.db.cursor() as cur:
        data = {"data": []}

        if use_format == "full" or not use_format:
            cur.execute("SELECT stores.name,address,zip,city FROM store_addresses \
                INNER JOIN stores ON store_addresses.store=stores.id")
            for record in cur.fetchall():
                full_adress = record[1] + " " + record[3] + " " + record[2]
                data["data"].append({"name": record[0], "full_adress": full_adress})
        elif use_format == "name":
            cur.execute("SELECT name FROM stores")
            for record in cur.fetchall():
                data["data"].append({"name": record[0]})
        elif use_format == "compact":
            cur.execute("SELECT stores.name,city FROM store_addresses INNER JOIN stores ON store_addresses.store=stores.id")
            for record in cur.fetchall():
                data["data"].append({"name": record[0], "city": record[1]})
        else:
            raise HTTPException(status_code=422, detail="Wrong format request")
        return data


@app.get("/stores/{name}", tags=['Friviligt'])
def get_store(name, use_format: Optional[str] = None):
    """
    Denna funktion returnerar data (namn och
    fullständigt address) för en specifik butik, vald via namn. Om ett namn
    som inte finns i DB anges, returnera 404 Not Found
    """
    with app.db.cursor() as cur:
        data = {"data": []}
        if use_format == "full" or not use_format:
            cur.execute("SELECT stores.name,address,zip,city \
                FROM store_addresses INNER JOIN stores ON store_addresses.store=stores.id \
                WHERE name=%(name)s", {"name": name})
            record = cur.fetchone()
            if not record:
                raise HTTPException(status_code=404, detail="Name not found")

            full_adress = record[1] + " " + record[3] + " " + record[2]
            data["data"].append({"name": record[0], "full_adress": full_adress})
        elif use_format == "name":
            cur.execute("SELECT name FROM stores WHERE name=%(name)s", {"name": name})
            record = cur.fetchone()
            if not record:
                raise HTTPException(status_code=404, detail="Name not found")

            data["data"].append({"name": record[0]})
        elif use_format == "compact":
            cur.execute("SELECT stores.name,city \
                FROM store_addresses INNER JOIN stores ON store_addresses.store=stores.id \
                WHERE name=%(name)s", {"name": name})
            record = cur.fetchone()
            if not record:
                raise HTTPException(status_code=404, detail="Name not found")

            data["data"].append({"name": record[0], "city": record[1]})
        else:
            raise HTTPException(status_code=422, detail="Wrong format request")
        return data


@app.get("/sales/")
def get_sales():
    """
    P7 GET /sales: Denna endpoint ska returnera en lista över alla transaktioner, i denna format:
    "data": [
        {"store": "Store name",
        "timestamp": "yyyymmddThh:mm:ss",
        "sale_id": "uuid-for-the-transaction-here"},
        ...
        ]
    """
    with app.db.cursor() as cur:
        data = {"data": []}

        cur.execute("SELECT name, time, sales.id FROM sales INNER JOIN stores ON sales.store=stores.id")
        for row in cur.fetchall():
            orginal_time_format = '%Y-%m-%d %H:%M:%S'
            row_datetime = datetime.datetime.strptime(str(row[1]), orginal_time_format)
            compleded_datatime = datetime.datetime.strftime(row_datetime, '%Y%m%dT%H:%M:%S')
            data["data"].append({"store": row[0], "timestamp": compleded_datatime, "sale_id": row[2]})

        return data


@app.get("/sales/{sale_id}")
def get_sale(sale_id):
    """
    P15 GET /sales/{sale-id}}: Denna endpoint ska returnera
    detaljer om en specifik transaktion, i denna format:
    {"data":
        {
            "store": "Store name",
            "timestamp": "yyyymmddThh:mm:ss",
            "sale_id": "uuid-for-the-transaction-here",
            "products":
            [
                {
                    "name": "Product name",
                    "qty": quantity-as-int,
                },
                ...
            ]
        }
    }
    Om {sale-id} inte finns i databasen, endpointen ska
    returnera {404 Not Found}. Om inte {sale-id} är
    en giltig UUID så ska endpointen returnera {422 Unprocessable Entry}.
    """

    if not is_valid_uuid(sale_id):
        raise HTTPException(status_code=422, detail="Wrong format on sales id, it need to be uuid valid")

    with app.db.cursor() as cur:
        data = {"data": []}

        cur.execute("SELECT stores.name, time, sales.id, products.name, sold_products.quantity FROM sales \
            INNER JOIN stores ON sales.store=stores.id \
            INNER JOIN sold_products ON sold_products.sale=sales.id \
            INNER JOIN products ON sold_products.product=products.id \
            WHERE sales.id=%(sale_id)s", {"sale_id": sale_id})
        rows = cur.fetchall()

        if not rows:
            raise HTTPException(status_code=404, detail="Sale not found")

        store = None
        sale_id = None
        compleded_datatime = None
        products = []
        first = True
        for row in rows:
            if first:
                orginal_time_format = '%Y-%m-%d %H:%M:%S'
                sales_datetime = datetime.datetime.strptime(str(row[1]), orginal_time_format)
                compleded_datatime = datetime.datetime.strftime(sales_datetime, '%Y%m%dT%H:%M:%S')

                store = row[0]
                sale_id = row[2]

                first = False

            products.append({"name": row[3], "qty": row[4]})

        data["data"].append({"store": store, "timestamp": compleded_datatime, "sale_id": sale_id, "products": products})

        return data


# QueryResultIncome is a named tuple used to ease the parsing of
# list-of-lists data format returned by cursor.fetchall into dictionaries
# ready to be returned as JSON.
QueryResultIncome = namedtuple("QueryResultIncome",
                               ("store_name", "product_name", "price",
                                "quantity", "sale_time", "discount"))


@app.get("/income")
def get_income(store: Optional[List[str]] = Query(None),
               product: Optional[List[str]] = Query(None),
               from_=Query(None, alias="from"), to_=Query(None, alias="to")):
    """GET /income

    Returns data in the usual format {"data": ·list-of-dicts}. Each
    dictionary contains all info about a transaction, including price and
    discount percent.

    It accepts the following query parameters:
        - store: (can be given more than once) UUID to filter results by store
        - product: (can be given more than once) UUID to filter results by
          product
        - from: filter out all transactions before the given datestamp/timestamp
        - to: filter out all transactions after the given datestamp/timestamp

    If any invalid UUID is given (either in store or product), 422 -
    Unprocessable Entity will be returned
    """
    stores_clause, products_clause, from_clause, to_clause = "", "", "", ""
    parameters = []
    if store:
        try:
            for iterator in store:
                uuid.UUID(iterator)
        except ValueError as err:
            raise HTTPException(status_code=422,
                                detail="Invalid UUID given for store!") from err
        stores_clause = "WHERE stores.id = ANY(%s)"
        parameters.append(store)
    if product:
        try:
            for iterator in product:
                uuid.UUID(iterator)
        except ValueError as err:
            raise HTTPException(
                status_code=422,
                detail="Invalid UUID given for product!") from err
        products_clause = "WHERE products.id = ANY(%s)"
        if parameters:
            products_clause = products_clause.replace("WHERE", "AND")
        parameters.append(product)
    if from_:
        from_clause = "WHERE sales.time >= %s"
        if parameters:
            from_clause = from_clause.replace("WHERE", "AND")
        parameters.append(from_)
    if to_:
        to_clause = "WHERE sales.time <= %s"
        if parameters:
            to_clause = to_clause.replace("WHERE", "AND")
        parameters.append(to_)
    query = """SELECT stores.name, products.name, prices.price,
               sold_products.quantity, sales.time, discounts.discount_percent
               FROM sold_products
               JOIN products on sold_products.product = products.id
               JOIN sales ON sold_products.sale = sales.id
               JOIN stores ON sales.store = stores.id
               JOIN prices ON products.id = prices.product
               LEFT JOIN discounts ON products.id = discounts.product
               {stores} {products} {from_} {to}
               ORDER BY sales.time;"""
    query = query.format(stores=stores_clause, products=products_clause,
                         from_=from_clause, to=to_clause)
    try:
        with app.db.cursor() as cur:
            cur.execute(query, parameters)
            result = cur.fetchall()
    except psycopg.errors.Error as err:
        app.db.rollback()
        raise HTTPException(status_code=422,
                            detail="Invalid datetime format!") from err
    entries = [QueryResultIncome(*r)._asdict() for r in result]
    return {"data": entries}


# QueryResultInventory is a named tuple used to ease the parsing of
# list-of-lists data format returned by cursor.fetchall into dictionaries
# ready to be returned as JSON.
QueryResultInventory = namedtuple("QueryResultInventory", ("product_name",
                                                           "adjusted_quantity",
                                                           "store_name"))


@app.get("/inventory")
def get_inventory(store=None, product=None):
    # This code we got from teacher in this project to use and fix all
    # problems with it, that why there are comment about changes we made to fix it
    """GET /inventory

    Returns data in the usual format {"data": ·list-of-dicts}. Each
    dictionary contains all info about *current* inventory (inventory -
    sales) showing inventory status per product and store.

    It accepts the following query parameters:
        - store: UUID to filter results by store
        - product: UUID to filter results by product

    If any invalid UUID is given (either in store or product), 422 -
    Unprocessable Entity will be returned

    """
    store_clause, product_clause = "", ""
    parameters = []
    if store:
        try:
            uuid.UUID(store)
        except ValueError as err:
            raise HTTPException(status_code=422,
                                detail="Invalid UUID for store!") from err  # Change "product" to "store"
        store_clause = "WHERE stores.id = %s"
        parameters.append(store)
    if product:
        try:
            uuid.UUID(product)
        except ValueError as err:
            raise HTTPException(status_code=422,
                                detail="Invalid UUID for product!") from err  # Change "store" to "product"
        product_clause = "WHERE products.id = %s"
        if parameters:  # Removed not so it checks if parameters has something.
            product_clause = product_clause.replace("WHERE", "AND")  # Change END to AND
        parameters.append(product)

    # Change from + to - in row "SUM(inventory.quantity) - SUM(sold_products.quantity),"
    # Add new line "JOIN sales ON sales.store = stores.id"
    # Add "AND sold_products.sale = sales.id" at end of "JOIN sold_products" row
    query = """SELECT products.name,
               SUM(inventory.quantity) - SUM(sold_products.quantity),
               stores.name
               FROM inventory
               JOIN products ON products.id = inventory.product
               JOIN stores ON stores.id = inventory.store
               JOIN sales ON sales.store = stores.id
               JOIN sold_products ON sold_products.product = products.id AND sold_products.sale = sales.id
               {store} {product}
               GROUP BY stores.name, products.name;
    """
    query = query.format(store=store_clause, product=product_clause)
    with app.db.cursor() as cur:
        cur.execute(query, parameters)
        result = cur.fetchall()
    entries = [QueryResultInventory(*r)._asdict() for r in result]
    return sorted(entries, key=lambda x: (x["store_name"], x["product_name"]))
